package herokuapp_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import herokuapp_pages.JavaScriptAlertPage;
import herokuapp_pages.NavigateToHerokuapp;

public class TestHerokuapp {

	WebDriver driver;

	/**
	 * This method is used to initiate the driver, and login into the website under test
	 */

	@BeforeTest
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "src/chrome_driver/chromedriver");

		//New instance of the chrome driver
		driver = new ChromeDriver();

		//Navigate to the website
		driver.navigate().to("http://the-internet.herokuapp.com/");
	}
	
	/*
	 * This test case will navigate to the JavaScript Alert page 
	 * And validate that the result message is correct as expected
	 */

	@Test 
	public void test_alerts() {
		
		NavigateToHerokuapp page = PageFactory.initElements(driver, NavigateToHerokuapp.class);

		page.navigateToJavaScriptAlerts();
		
		JavaScriptAlertPage page1 = PageFactory.initElements(driver, JavaScriptAlertPage.class);
		page1.clickOnAlerts();
		driver.switchTo().alert().accept();

		//Check that result message is correct

		String expectedString = "You clicked: Ok";
		String actualString = page1.getResultElement();

		Assert.assertEquals(expectedString, actualString);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}