package herokuapp_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class JavaScriptAlertPage {
	
	@FindBy (css = "#content > div > ul > li:nth-child(2) > button" )
	WebElement alerts;
	
	@FindBy (xpath = "//*[@id=\"result\"]")
	WebElement resultElement;
	
	/**
	 * This POM method is used in the test case to click on the
	 * Alerts button on the page
	 */
	
	public void clickOnAlerts() {
		
		alerts.click();
		
	}
	/**
	 * This POM method used in the test case 
	 * go validate the result text message
	 * @return
	 */
	
	public String getResultElement() {
		
		return resultElement.getText(); 
	}
}	