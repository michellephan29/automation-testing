package herokuapp_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigateToHerokuapp {
	
	@FindBy (css = "#content > ul > li:nth-child(29) > a" )
	WebElement javaScriptAlertLink;
	
	/**
	 * This POM method, is used in the test case to 
	 * Click on the java script alert link on the page
	 * @param driver
	 */

	public void navigateToJavaScriptAlerts() {
		
		javaScriptAlertLink.click();
	}
}
