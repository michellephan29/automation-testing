package palindrome;
import org.junit.Assert;

public class PalindromeTest {
	
public static void main(String[] args) {
	
	//The below unit test covers both positive and negative secenarios
        
	String String1 = "Anna";
	String String2 = "CiVic";
	String String3 = "KayAK";
	String String4 = "Level";
	String String5 = "Madam";
	String String6 = "Mom";
	String String7 = "Noon";
	String String8 = "Racecar";
	String String9 = "Radar";
	String String10 = "Redder";
	String String11 = "Refer";
	String String12 = "Repaper";
	String String13 = "Rotator";
	String String14 = "Rotor";
	String String15 = "Sagas";
	String String16 = "Solos";
	String String17 = "Stats";
	String String18 = "Tenet";
	String String19 = "Wow";
	String String20 = "Michelle";
	String String21 = "Sunday";
		
	Palindrome string = new Palindrome();

	Assert.assertTrue(string.isPalindrome(String1));
	Assert.assertTrue(string.isPalindrome(String2));
	Assert.assertTrue(string.isPalindrome(String3));
	Assert.assertTrue(string.isPalindrome(String4));
	Assert.assertTrue(string.isPalindrome(String5));
	Assert.assertTrue(string.isPalindrome(String6));
	Assert.assertTrue(string.isPalindrome(String7));
	Assert.assertTrue(string.isPalindrome(String8));
	Assert.assertTrue(string.isPalindrome(String9));
	Assert.assertTrue(string.isPalindrome(String10));
	Assert.assertTrue(string.isPalindrome(String11));
	Assert.assertTrue(string.isPalindrome(String12));
	Assert.assertTrue(string.isPalindrome(String13));
	Assert.assertTrue(string.isPalindrome(String14));
	Assert.assertTrue(string.isPalindrome(String15));
	Assert.assertTrue(string.isPalindrome(String16));
	Assert.assertTrue(string.isPalindrome(String17));
	Assert.assertTrue(string.isPalindrome(String18));
	Assert.assertTrue(string.isPalindrome(String19));  
	Assert.assertFalse(string.isPalindrome(String20));
	Assert.assertFalse(string.isPalindrome(String21));
    }

}
