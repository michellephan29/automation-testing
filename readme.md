# Technical Assessment
1. Write a function that validates palindromes
2. Write a test automation script that goes to the website (http://the-internet.herokuapp.com/) and follows the required actions

## Instructions 
1. The palindromes code, is located in the path src/palindrome

	a) Code is in the 'Palindrome' class
	
	b) Unit test code is in the PalindromeTest class
	
	
2. The automation script is located in the path src/pages & src/tests. It utilises the page object model patten, hence it follows the below:

	a) The pages package (src/pages) contains all of the web elements and the methods to operate the web elements on the website
	
	b) The test page (src/tests) contains the verification methods
	
	c) To run the script, the testng.xml is utilised



